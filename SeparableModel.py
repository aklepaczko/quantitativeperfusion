"""

@author: Artur Klepaczko (C) 2020 Lodz University of Technology

The 2-Compartment Separable Model proposed by  Sourbron

"""

import numpy as np


def tissue_plasma_conc(time_window_phase, aif, T_p):
    dt = time_window_phase[1]
    tissue_plasma_concentration = dt * np.convolve(np.exp(-time_window_phase / T_p),
                                                   aif,
                                                   mode='full') / T_p
    return tissue_plasma_concentration


def concentration_separable_model(time_window_phase, F_T, V_p, T_T, T_P, **params):
    aif = params['aif']
    dt = time_window_phase[1]
    tissue_plasma_concentration = tissue_plasma_conc(time_window_phase, aif, T_P)
    exponent = np.exp(-time_window_phase / T_T)
    ev_compartment = dt * F_T * np.convolve(exponent,
                                            tissue_plasma_concentration[:len(aif)],
                                            mode='full')

    iv_compartment = V_p * tissue_plasma_concentration[:len(aif)]

    tissue_concentrations_model = ev_compartment[:len(iv_compartment)] + iv_compartment

    return tissue_concentrations_model[:len(time_window_phase)]
