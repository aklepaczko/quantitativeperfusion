import numpy as np
from PkModeling import PkModeling2CFM


class PkMapGenerator:

    def __init__(self, pk_modeling: PkModeling2CFM, input_img, input_roi):
        self.pkModeling = pk_modeling
        self.inputImg = input_img
        self.inputRoi = input_roi
        self.outputImg = np.zeros_like(input_roi, dtype=float)

    def fit(self, aif):

        # get indices of voxels in roi mask
        roi = self.inputRoi
        (ir, ic, islice) = np.where(roi > 0)
        num_roi_points = len(ir)
        print('Performing PK model fitting for %d voxels in the roi provided.' % num_roi_points)
        for i in range(num_roi_points):
            signal = self.inputImg[ir[i], ic[i], islice[i], :]
            # assert that len(aif)==len(signal)
            self.pkModeling.fit(aif, signal)
            self.outputImg[ir[i], ic[i], islice[i]] = self.pkModeling.Ktrans
            if (i % 10) == 0:
                print('Done %d / %d voxels.' % (i, num_roi_points))

    def predict(self):
        return self.outputImg
