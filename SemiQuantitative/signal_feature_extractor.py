import numpy as np
import pywt
from scipy.stats import skew, kurtosis
from math import sqrt


def get_feature_names(prefix: str):
    names = ['Mean', 'StdDev', 'Skewness', 'Kurtosis', 'RMS', 'FormFactor',
             'Energy', 'ShanonEntropy', 'LogEnergyEntropy', 'IQR']
    return list(map(lambda x: f'{prefix:s}_' + x, names))


def get_semi_quantitative_features(signal):
    mean_baseline = np.mean(signal[:5])
    bat_time = next((i for i, x in enumerate(signal) if x > 1.1 * mean_baseline), None) - 1
    max_signal = np.max(signal[:bat_time + 10])
    peak_time = np.argmax(signal[:bat_time + 10])
    auc_time = next((i for i, x in enumerate(signal[peak_time:]) if x < 0.8 * max_signal), None) + peak_time
    return bat_time, peak_time, auc_time


def calculate_feature_vector(coeff: np.ndarray):
    mu = np.mean(coeff)
    rms = sqrt(np.sum(coeff**2)/len(coeff))
    # mean, standard dev., skewness, kurtosis, rms, form factor, energy,
    # shanon entropy, log-energy entropy, IQR
    vector = [mu,
              np.std(coeff),
              skew(coeff),
              kurtosis(coeff),
              rms,
              mu/rms,
              np.square(np.sum(np.abs(coeff))),
              -np.sum([x**2*np.log(x**2) for x in coeff]),
              np.sum(np.log(coeff**2)),
              np.percentile(coeff, 75)-np.percentile(coeff, 25)]
    return vector


def get_wavelet_features2(signal, *, method, decomp_level=''):
    output_features = []
    feature_names = []
    coefficients = pywt.dwt(signal, method, 'smooth')
    output_features.extend(calculate_feature_vector(coefficients[0]))
    feature_names.extend(get_feature_names(method + f'_{decomp_level:s}L'))
    output_features.extend(calculate_feature_vector(coefficients[1]))
    feature_names.extend(get_feature_names(method + f'_{decomp_level:s}H'))
    return coefficients[0], output_features, feature_names


def get_wavelet_features(signal, *, wavelet_fun='db1'):
    feature_names = []
    coeff1, features1, feature_names1 = get_wavelet_features2(signal, method=wavelet_fun)
    feature_names.extend(feature_names1)
    coeff2, features2, feature_names2 = get_wavelet_features2(coeff1, method=wavelet_fun, decomp_level='L')
    feature_names.extend(feature_names2)
    _, features3, feature_names3 = get_wavelet_features2(coeff2, method=wavelet_fun, decomp_level='LL')
    feature_names.extend(feature_names3)
    return features1, features2, features3, feature_names


def get_wavelet_coefficients(signal, *, method, decomp_level=''):
    coefficients = pywt.dwt(signal, method, 'smooth')
    feature_names = [method + f'_{decomp_level:s}L_coeff{x:d}' for x in range(len(coefficients[0]))]
    feature_names_h = [method + f'_{decomp_level:s}H_coeff{x:d}' for x in range(len(coefficients[1]))]
    feature_names.extend(feature_names_h)
    return coefficients, feature_names


def get_wavelet_decomposition(signal, *, wavelet_fun='db1'):
    feature_names = []
    coeff1, feature_names1 = get_wavelet_coefficients(signal, method=wavelet_fun)
    feature_names.extend(feature_names1)
    coeff2, feature_names2 = get_wavelet_coefficients(coeff1[0], method=wavelet_fun, decomp_level='L')
    feature_names.extend(feature_names2)
    coeff3, feature_names3 = get_wavelet_coefficients(coeff2[0], method=wavelet_fun, decomp_level='LL')
    feature_names.extend(feature_names3)
    return (np.concatenate((coeff1[0], coeff1[1])), np.concatenate((coeff2[0], coeff2[1])),
            np.concatenate((coeff3[0], coeff3[1])), feature_names)
