"""

@author: Artur Klepaczko (C) 2019 Lodz University of Technology

The 2-Compartment Filtration Toft's Model

"""
import numpy as np
from scipy.integrate import cumtrapz
from scipy.integrate import trapz


def _time_delayed_exponential_virf(time_window_phase, length_of_virf, Delta, delay_point, Tg):
    virf = np.zeros(length_of_virf)
    invTg = 1 / Tg

    for i in range(delay_point, length_of_virf):
        virf[i] = np.exp(-invTg * (time_window_phase[i] - Delta))

    dt = time_window_phase[1]
    virf = virf / (trapz(virf, dx=dt))
    return virf


def tissue_plasma_conc(time_window_phase, aif, length_of_virf, Delta, Tg):
        dt = time_window_phase[1]
        delay_point = int(round(Delta / dt))
        virf = _time_delayed_exponential_virf(time_window_phase, length_of_virf, Delta, delay_point, Tg)
        tissue_plasma_concentration = dt * np.convolve(aif, virf, mode='full')
        return tissue_plasma_concentration


def concentration_filtration_model(time_window_phase, Ktrans, vb, Delta, Tg, **params):
    aif = params['aif']
    length_of_virf = params['length_of_virf']
    dt = params['temporal_resolution']
    hct_small = params['hct_small']
    tissue_plasma_concentration = tissue_plasma_conc(time_window_phase, aif, length_of_virf, Delta, Tg)

    ev_compartment = Ktrans * cumtrapz(tissue_plasma_concentration, dx=dt)
    iv_compartment = (1 - hct_small) * vb * tissue_plasma_concentration

    tissue_concentrations_model = ev_compartment + iv_compartment[:len(ev_compartment)]

    return tissue_concentrations_model[:len(time_window_phase)]
