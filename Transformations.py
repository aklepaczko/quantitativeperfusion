"""

@author Artur Klepaczko (C) 2020 Lodz University of Technology

"""

import numpy as np
from scipy import interpolate as intrp
from typing import Tuple


def interpolate(signals: np.ndarray, dt: float, dt_interp: float) -> Tuple[np.ndarray, np.ndarray]:
    """
    Interpolates input signal to the dt_interp temporal resolution from the base dt resolution
    :param dt: float
        Input temporal resolution, seconds
    :param signals: 2D array of float
        Signals to interpolate. Each column represents a successive time step. Every row is
        a separate signal.
    :param dt_interp: float
        Target temporal resolution, seconds
    :return: tuple of arrays
        Resampled time window, interpolated signals array
    """
    signals = np.array(signals, ndmin=2)
    num_signals, num_time_frames = signals.shape

    time_window = np.arange(0, dt * num_time_frames, dt)

    time_window_interpolated = np.linspace(0, time_window[-1], int(round(time_window[-1] / dt_interp)))

    signals_interp = []
    for signal in signals:
        tck = intrp.splrep(time_window, signal, s=0)
        signals_interp.append(intrp.splev(time_window_interpolated, tck, der=0))

    signals_interp = signals_interp[0] if num_signals == 1 else np.array(signals_interp)
    return time_window_interpolated, np.array(signals_interp)


class MRITransformation:
    """ Utility class for performing signal to concentration conversion
        and interpolation.

    Attributes
    ----------

    TR : float
        MR sequence repetition time, seconds

    FA : float
        MR sequence flip angle. degrees

    dt : float
        Temporal resolution of the DCE-MRI sequence

    Methods
    -------

    convert_signal_to_concentration
        Converts signal time curve to contrast agent concentration time
        curve assuming Gradient Echo acquisition sequence

    interpolate
        Interpolates signal to achieve desired temporal resolution

    """

    def __init__(self, mri_params, dt):
        """
        Creates MRITransformation object

        :param mri_params: dict of {str: float}
            DCE-MRI sequence parameters. Values should be provided for
            each of the required keys:
            - "TR" - repetition time in seconds
            - "FA" - flip angle in degrees

        :param dt: float
            Base temporal resolution, seconds
        """

        self.TR = mri_params.get("TR")
        self.flip_angle = mri_params.get("FA")
        self.sin_a = np.sin(self.flip_angle * np.pi / 180)
        self.cos_a = np.cos(self.flip_angle * np.pi / 180)
        self.dt = dt

    def signal_to_concentration(self, signal, BAT, phase_time, T_10, relaxivity=4.5):
        """
        Converts signal time curve to contrast agent concentration time
        curve assuming Gradient Echo acquisition sequence

        :param signal: array of floats
            signal samples in time, required
        :param BAT: float
            Bolus arrival time, seconds
        :param phase_time: float
            Time range of analysis, seconds
        :param relaxivity: float, optional, default: 4.5 sec^-1 mM^-1
            Tissue and blood longitudinal relaxivity constant
        :param T_10:  float, optional, default: 1.2 sec
            Longitudinal relaxation time of tissue before bolus arrival
        :return: array of floats
            Time course of contrast agent concentration
        """
        concentration = np.zeros(phase_time)
        TR = self.TR
        invTR = 1 / TR
        b = (1 - np.exp(-TR / T_10)) / (1 - self.cos_a * np.exp(-TR / T_10))
        S0 = np.mean(signal[:BAT])
        for i in range(0, phase_time):
            a_t = signal[i] / S0
            R1_t = -invTR * np.log((1 - a_t * b) / (1 - a_t * b * self.cos_a))
            concentration[i] = (R1_t - 1 / T_10) / relaxivity
        return concentration

    def concentration_to_signal(self, concentration, T_10, signal_baseline, relaxivity=4.5):
        TR = self.TR
        R1kidney_t = 1/T_10 + relaxivity * concentration
        power_t = -TR * R1kidney_t
        b = (1 - np.exp(-TR / T_10) * self.cos_a) / ((1 - np.exp(-TR / T_10)) * self.sin_a)
        signal_model = signal_baseline * b * (1 - np.exp(power_t)) * self.sin_a / (1 - np.exp(power_t) * self.cos_a)
        return signal_model

    # def interpolate(self, signal, dt_interp):
    #     """
    #     Interpolates input signal to the dt_interp temporal resolution from the
    #     base self.dt resolution
    #     :param signal: array of float
    #         Signal to interpolate
    #     :param dt_interp: float
    #         Target temporal resolution, seconds
    #     :return: array of float
    #         Interpolated signal
    #     """
    #     num_time_frames = len(signal)
    #     time_window = np.arange(0, self.dt * num_time_frames, self.dt)
    #
    #     time_window_interpolated = np.linspace(0, time_window[-1], int(round(time_window[-1] / dt_interp)))
    #
    #     tck = intrp.splrep(time_window, signal, s=0)
    #     signal_interp = intrp.splev(time_window_interpolated, tck, der=0)
    #     return time_window_interpolated, signal_interp
