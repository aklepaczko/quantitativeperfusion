#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author Artur Klepaczko (C) 2019 Lodz University of Technology

Demonstration of using the pharmacokinetic modeling class.

"""
from PkModeling import PkModeling2CFM, PkModeling2SCM
import matplotlib.pyplot as plt
import numpy as np
import sys


def pk_model_2cfm(**kwargs):
    return PkModeling2CFM(**kwargs)


def pk_model_2csm(**kwargs):
    return PkModeling2SCM(**kwargs)


def create_model(model_type: str):
    models = {'2cfm': pk_model_2cfm,
              '2csm': pk_model_2csm}
    return models.get(model_type, '2cfm')


def run_pk_experiment(*, aif_file_name: str, signal_file_name: str, side: str, roi: str,
                      phase_frame: int, bat_frame: int, model: str = '2cfm'):
    aif = np.load(aif_file_name)
    signals = np.load(signal_file_name)

    curve_ids = {('left', 'cortex'): 0,
                 ('right', 'cortex'): 1,
                 ('left', 'kidney'): 2,
                 ('right', 'kidney'): 3}

    signal = signals[curve_ids.get((side, roi)), :]

    temporalResolutionImage = 2.3  # sec
    temporalResolutionInterp = 0.4  # sec

    flipAngle = 20
    TR = 2.36e-3
    T_10_blood = 1.4  # sec
    T_10_kid = 1.2  # sec
    relaxivity = 4.5

    parameters = {'TR': TR,
                  'FA': flipAngle}

    pk_model_constructor = create_model(model)
    pk_model = pk_model_constructor(bat_frame=bat_frame, phase_frame=phase_frame,
                                    dt=temporalResolutionImage, mri_params=parameters,
                                    T_10=T_10_blood, T_10_tissue=T_10_kid, relaxivity=relaxivity,
                                    dt_interp=temporalResolutionInterp)

    pk_model.fit(aif, signal)

    (signal_by_model, iv_compartment, ev_compartment) = pk_model.predict()

    numPoints = len(pk_model.aif_conc)
    timeScale = np.arange(0, 10 * temporalResolutionInterp * numPoints / 10, temporalResolutionInterp)

    fig1 = plt.figure()
    plt.plot(timeScale, pk_model.aif_interp[:numPoints], color='m', label='AIF')
    plt.plot(timeScale, signal_by_model[:numPoints], color='r', label='Model')

    timeScaleOriginal = np.arange(0, temporalResolutionImage * phase_frame,
                                  temporalResolutionImage)

    plt.plot(timeScaleOriginal, signal[:phase_frame], color='grey',
             marker='+', markersize=6, linestyle='None', label='Image original')
    plt.legend()
    plt.xlabel('time [s]')
    plt.ylabel('signal [a.u.]')
    plt.title('The two-compartment filtration model - signal domain')
    plt.show()

    plt.figure()
    plt.plot(timeScale, pk_model.aif_conc, color='m', label='AIF')
    plt.plot(timeScale, ev_compartment[:numPoints], color='g', label='EV_compartment')
    plt.plot(timeScale, iv_compartment[:numPoints], color='b', label='IV_compartment')
    tissue_concentrations_model = ev_compartment + iv_compartment[:len(ev_compartment)]
    plt.plot(timeScale, tissue_concentrations_model[:numPoints], color='r', label='Model')

    plt.legend()
    plt.xlabel('time [s]')
    plt.ylabel('GdDOTA concentration [a.u.]')
    plt.title('The two-compartment filtration model - tracer concentration domain')
    plt.show()


if __name__ == '__main__':
    run_pk_experiment(aif_file_name=sys.argv[1], signal_file_name=sys.argv[2],
                      side=sys.argv[3], roi=sys.argv[4], phase_frame=34, bat_frame=7, model='2csm')
