"""

@author Artur Klepaczko (C) 2020 Lodz University of Technology

"""
import numpy as np
import Tofts2CFModel
import SeparableModel
from scipy.optimize import curve_fit
from scipy.integrate import cumtrapz
from Transformations import MRITransformation, interpolate


class PkModeling2SCM:
    __slots__ = ('Ft', 'Vp', 'Tt', 'Tp', '_bat_frame',
                 '_phase_frame', '_dt', '_dt_interp', '_mri_params',
                 '_T_10', '_T_10_tissue', '_R_10_tissue', '_relaxivity',
                 '_hct_large', '_hct_small', '_verbose', '_phase_time',
                 '_BAT', '_optimized', '_transformation', '_time_window_upsampled',
                 '_Ft0', '_Vp0', '_Tt0', '_Tp0', '_aif_conc', '_aif_interp', '_signal_interp')

    """
    Pharmacokinetic modeling using Sourbron's 2-compartment separable model

    Attributes
    ----------
    _Ft : float
    _Vp : float
    _Tt : float, seconds
    _Tp : float, seconds

    _aif_conc : numpy.ndarray
        Arterial input function in DCE-MRI sequence temporal resolution

    _aif_interp : numpy.ndarray
        Arterial input function in interpolated temporal resolution

    _signal_interp : numpy.ndarray
        Modelled signal in interpolated temporal resolution
    """

    def __init__(self, *, bat_frame, phase_frame, dt, mri_params,
                 T_10=1.2, T_10_tissue=1.2, relaxivity=4.5,
                 dt_interp=0.4, hct_large=0.4, hct_small=0.25,
                 Ft0=0.66, Vp0=15, Tt0=15, Tp0=4.5,
                 verbose=0):
        """
        Parmeters
        ---------

        :param bat_frame : int
            Frame number of the bolus arrival time
        :param phase_frame : int
            Frame number indicating the end of the perfusion (or uptake) phase
        :param dt : float
            Temporal resolution of the DCE-MRI sequence
        :param mri_params : dict of {str: float}
            DCE-MRI sequence parameters. Values should be provided for each of the required keys:
                - "TR" - repetition time in seconds
                - "FA" - flip angle in degrees
        :param T_10 : float, optional, default: 1.2 sec
            Longitudinal relaxition time of blood before bolus arrival
        :param T_10_tissue : float, optional, default: 1.2 sec
            Longitudinal relaxation time of tissue before bolus arrival
        :param relaxivity : float, optional, default: 4.5 sec^-1 mM^-1
            Tissue and blood longitudinal relaxivity constant
        :param dt_interp : float, optional, default: 0.4 sec
            Interpolated temporal resolution
        :param hct_large : float, optional, default: 0.41
            Hematocrit value in large arteries
        :param hct_small : float, optional, default: 0.25
            Hematocrit value in arterioles
        :param Ft0 : float, optional, default: 0.66 mL / min / 100 mL tissue
            Initial guess for ... #TODO Check interpretation
        :param Vp0 : float, optional, default: 15 mL / 100 mL
            Initial guess for ...
        :param Tt0 : float, optional, default: 15 seconds
            Initial guess for ...
        :param Tp0 : float, optional, default: 4.5 seconds
            Initial guess for ...
        :param verbose:
        """

        self._bat_frame = bat_frame
        self._phase_frame = phase_frame
        self._dt = dt
        self._dt_interp = dt_interp
        self._mri_params = mri_params
        self._T_10 = T_10
        self._T_10_tissue = T_10_tissue
        self._R_10_tissue = 1 / T_10_tissue
        self._relaxivity = relaxivity
        self._hct_large = hct_large
        self._hct_small = hct_small
        self._verbose = verbose
        self._phase_time = round(phase_frame * dt / dt_interp)
        self._BAT = round(bat_frame * dt / dt_interp)
        self._optimized = False
        self._transformation = MRITransformation(mri_params, self._dt)
        self._time_window_upsampled = None
        self._Ft0 = Ft0
        self._Vp0 = Vp0
        self._Tt0 = Tt0
        self._Tp0 = Tp0
        self._aif_conc = None
        self._aif_interp = None
        self._signal_interp = None
        self.Ft = None
        self.Vp = None
        self.Tt = None
        self.Tp = None

    def fit(self, aif, signal):
        self._time_window_upsampled, aif_interp = interpolate(aif, self._dt, self._dt_interp)
        _, signal_interp = interpolate(signal, self._dt, self._dt_interp)
        aif_conc = self._transformation \
            .signal_to_concentration(aif_interp, self._BAT, self._phase_time,
                                     self._T_10, self._relaxivity)
        aif_conc = aif_conc / (1 - self._hct_large)
        lb = [0, 0, 15, 1]
        ub = [10, 40, 500, 10]

        def _func(x, Ft, Vp, Tt, Tp):
            tissue_concentration = SeparableModel.concentration_separable_model(x, Ft, Vp, Tt, Tp, aif=aif_conc)
            S0 = np.mean(signal_interp[:self._BAT])
            signal_model = self._transformation.concentration_to_signal(tissue_concentration, self._T_10_tissue, S0,
                                                                        self._relaxivity)
            return signal_model

        x0 = [self._Ft0, self._Vp0, self._Tt0, self._Tp0]

        (Ft1, Vp1, Tt1, Tp1), pcov = curve_fit(_func,
                                               self._time_window_upsampled[:self._phase_time],
                                               signal_interp[:self._phase_time],
                                               method='trf',
                                               p0=x0,
                                               bounds=(lb, ub))

        self.Ft = Ft1
        self.Vp = Vp1
        self.Tt = Tt1
        self.Tp = Tp1
        self._aif_conc = aif_conc
        self._aif_interp = aif_interp
        self._signal_interp = signal_interp
        self._optimized = True

    def predict(self):
        if not self._optimized:
            print("Warning! Model is not in the optimized state. Results are undefined.")

        tissue_plasma_concentration = SeparableModel.tissue_plasma_conc(
            self._time_window_upsampled[:self._phase_time],
            self._aif_conc, self.Tp)
        exponent = np.exp(-self._time_window_upsampled[:self._phase_time] / self.Tt)
        ev_compartment = self._dt_interp * self.Ft * np.convolve(exponent,
                                                                 tissue_plasma_concentration[:len(self._aif_conc)],
                                                                 mode='full')
        iv_compartment = self.Vp * tissue_plasma_concentration[:len(self._aif_conc)]
        ev_compartment = ev_compartment[:len(iv_compartment)]

        tissue_concentrations_model = ev_compartment + iv_compartment
        S0 = np.mean(self._signal_interp[:self._BAT])
        signal_model = self._transformation.concentration_to_signal(tissue_concentrations_model, self._T_10_tissue, S0,
                                                                    self._relaxivity)
        return signal_model, iv_compartment, ev_compartment

    @property
    def aif_conc(self):
        return self._aif_conc

    @property
    def aif_interp(self):
        return self._aif_interp

    @property
    def local_gfr(self):
        return self.Ft * 60 / 1000


class PkModeling2CFM:
    __slots__ = ('_bat_frame', '_phase_frame', '_dt', '_dt_interp', '_mri_params',
                 '_T_10', '_T_10_tissue', '_R_10_tissue', '_relaxivity', '_hct_large',
                 '_hct_small', '_fpeak0', '_vb0', '_ktrans0', '_delay_time0', '_verbose',
                 '_phase_time', '_BAT', '_time_of_virf', '_optimized', '_transformation',
                 '_time_window_upsampled', '_Ktrans', '_vb', '_Delta', '_Tg', '_aif_conc',
                 '_aif_interp', '_signal_interp', '_length_of_virf')

    """ Pharmacokinetic modeling using the Tofts 2-compartment filtration model

    Attributes
    ----------

    _Ktrans : float
    _vb : float
    _Delta : float
    _Tg : float
        Parameters of the optimized model

    _aif_conc : numpy.ndarray
        Arterial input function in DCE-MRI sequence temporal resolution

    _aif_interp : numpy.ndarray
        Arterial input function in interpolated temporal resolution

    _signal_interp : numpy.ndarray
        Modelled signal in interpolated temporal resolution

    _length_of_virf : int
        Number of frames of the vascular impulse response function
    """

    def __init__(self, *, bat_frame, phase_frame, dt, mri_params,
                 T_10=1.2, T_10_tissue=1.2, relaxivity=4.5,
                 dt_interp=0.4, hct_large=0.4, hct_small=0.25,
                 Fpeak0=9.033, vb0=0.41, Ktrans0=0.25, delayTime0=2,
                 time_of_virf=20.0,
                 verbose=0):
        """
        Parmeters
        ---------

        :param bat_frame : int
            Frame number of the bolus arrival time
        :param phase_frame : int
            Frame number indicating the end of the perfusion (or uptake) phase
        :param dt : float
            Temporal resolution of the DCE-MRI sequence
        :param mri_params : dict of {str: float}
            DCE-MRI sequence parameters. Values should be provided for each of the required keys:
                - "TR" - repetition time in seconds
                - "FA" - flip angle in degrees
        :param T_10 : float, optional, default: 1.2 sec
            Longitudinal relaxition time of blood before bolus arrival
        :param T_10_tissue : float, optional, default: 1.2 sec
            Longitudinal relaxation time of tissue before bolus arrival
        :param relaxivity : float, optional, default: 4.5 sec^-1 mM^-1
            Tissue and blood longitudinal relaxivity constant
        :param dt_interp : float, optional, default: 0.4 sec
            Interpolated temporal resolution
        :param hct_large : float, optional, default: 0.41
            Hematocrit value in large arteries
        :param hct_small : float, optional, default: 0.25
            Hematocrit value in arterioles
        :param Fpeak : float, optional, default: 9.033 mL / min / 100 mL tissue
            Peak renal plasma flow
        :param vb0 : float, optional, default: 0.41
            Inital guess for blood volume fraction
        :param Ktrans0 : float, optional, default: 0.25
            Initial guess for volume transfer constant
        :param delayTime0 : float, optional, default: 2 seconds
            Initial guess for the AIF delay
        :param time_of_virf : float, optional, default: 20.0 seconds
            Duration of the vascular impulse response function
        :param verbose:
        """

        self._bat_frame = bat_frame
        self._phase_frame = phase_frame
        self._dt = dt
        self._dt_interp = dt_interp
        self._mri_params = mri_params
        self._T_10 = T_10
        self._T_10_tissue = T_10_tissue
        self._R_10_tissue = 1 / T_10_tissue
        self._relaxivity = relaxivity
        self._hct_large = hct_large
        self._hct_small = hct_small
        self._fpeak0 = Fpeak0 / 100
        self._vb0 = vb0
        self._ktrans0 = Ktrans0 / 60
        self._delay_time0 = delayTime0
        self._verbose = verbose
        self._phase_time = round(phase_frame * dt / dt_interp)
        self._BAT = round(bat_frame * dt / dt_interp)
        self._time_of_virf = time_of_virf
        self._optimized = False
        self._transformation = MRITransformation(mri_params, self._dt)
        self._time_window_upsampled = None
        self._Ktrans = 0
        self._vb = 0
        self._Delta = 0
        self._Tg = 0
        self._aif_conc = None
        self._aif_interp = None
        self._signal_interp = None
        self._length_of_virf = 0

    def fit(self, aif, signal):
        self._time_window_upsampled, aif_interp = interpolate(aif, self._dt, self._dt_interp)
        _, signal_interp = interpolate(signal, self._dt, self._dt_interp)
        aif_conc = self._transformation \
            .signal_to_concentration(aif_interp, self._BAT, self._phase_time,
                                     self._T_10, self._relaxivity)
        aif_conc = aif_conc / (1 - self._hct_large)
        length_of_virf = round(self._time_of_virf / self._dt_interp)
        lb = [0, 0.2, 1, 1]
        ub = [0.4 / 60, 0.9, np.inf, np.inf]

        def _func(x, Ktrans, vb, Delta, Tg):
            tissue_concentration = Tofts2CFModel.concentration_filtration_model(x, Ktrans, vb, Delta, Tg,
                                                                                aif=aif_conc,
                                                                                length_of_virf=length_of_virf,
                                                                                temporal_resolution=self._dt_interp,
                                                                                hct_small=self._hct_small)

            S0 = np.mean(signal_interp[:self._BAT])
            signal_model = self._transformation.concentration_to_signal(tissue_concentration, self._T_10_tissue, S0,
                                                                        self._relaxivity)
            return signal_model

        delay_point0 = int(round(self._delay_time0 / self._dt_interp))
        Delta0 = self._time_window_upsampled[delay_point0 - 1]
        Tg0 = self._vb0 / self._fpeak0
        x0 = [self._ktrans0, self._vb0, Delta0, Tg0]

        (Ktrans1, vb1, Delta1, Tg1), _ = curve_fit(_func,
                                                   self._time_window_upsampled[:self._phase_time],
                                                   signal_interp[:self._phase_time],
                                                   method='trf',
                                                   p0=x0,
                                                   bounds=(lb, np.inf))

        self._Ktrans = Ktrans1
        self._vb = vb1
        self._Delta = Delta1
        self._Tg = Tg1
        self._aif_conc = aif_conc
        self._aif_interp = aif_interp
        self._signal_interp = signal_interp
        self._length_of_virf = length_of_virf
        self._optimized = True

    def predict(self):
        if not self._optimized:
            print("Warning! Model is not in the optimized state. Results are undefined.")

        tissue_plasma_concentration = Tofts2CFModel.tissue_plasma_conc(
            self._time_window_upsampled[:self._phase_time],
            self._aif_conc, self._length_of_virf, self._Delta, self._Tg
        )
        ev_compartment = self._Ktrans * cumtrapz(tissue_plasma_concentration, dx=self._dt_interp)
        iv_compartment = (1 - self._hct_small) * self._vb * tissue_plasma_concentration

        tissue_concentrations_model = ev_compartment + iv_compartment[:len(ev_compartment)]
        S0 = np.mean(self._signal_interp[:self._BAT])
        signal_model = self._transformation.concentration_to_signal(tissue_concentrations_model, self._T_10_tissue, S0,
                                                                    self._relaxivity)
        return signal_model, iv_compartment, ev_compartment

    @property
    def Ktrans(self):
        return self._Ktrans

    @property
    def aif_conc(self):
        return self._aif_conc

    @property
    def aif_interp(self):
        return self._aif_interp

    @property
    def local_gfr(self):
        return self.Ktrans * 60 / 1000
