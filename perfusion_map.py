#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@author Artur Klepaczko (C) 2020 Lodz University of Technology

Demonstration of using the pharmacokinetic modeling class.

"""

import numpy as np
import nibabel
import sys
from PkModeling import PkModeling2CFM
from PkMapGenerator import PkMapGenerator

aif_file_name = sys.argv[1]
image_file_name = sys.argv[2]
roi_file_name = sys.argv[3]

aif = np.load(aif_file_name)
img = nibabel.load(image_file_name).get_data()
roi = nibabel.load(roi_file_name)
roi_affine = roi.affine
roi = roi.get_data()

temporalResolutionImage = 2.3  # sec
temporalResolutionInterp = 0.4  # sec

flipAngle = 20
TR = 2.36e-3
T_10_blood = 1.4  # sec
T_10_kid = 1.2  # sec
relaxivity = 4.5

phase_frame = 34
bat_frame = 7

mri_params = {'TR': TR,
              'FA': flipAngle}

pk_model = PkModeling2CFM(bat_frame, phase_frame, temporalResolutionImage, mri_params,
                          T_10=T_10_blood, T_10_tissue=T_10_kid, relaxivity=relaxivity,
                          dt_interp=temporalResolutionInterp)

pk_map_generator = PkMapGenerator(pk_model, img, roi)

pk_map_generator.fit(aif)

perfusion_map = pk_map_generator.predict()

perfusion_map_nii = nibabel.Nifti1Image(perfusion_map, roi_affine)
nibabel.save(perfusion_map_nii, 'ExampleData/perfusion_map.nii')